### gitlab runner url
URL="https://gitlab.com/"
### gitlab runner token
TOKEN=""
### email for Let’s Encrypt
EMAIL=""

apt update && apt upgrade -y && apt install -y git tomcat9 nginx certbot python3-certbot-nginx curl

curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"
dpkg -i gitlab-runner_amd64.deb

gitlab-runner register --non-interactive --url $URL --registration-token $TOKEN \
  --executor shell --locked=true --run-untagged=false --tag-list shell --description "test-shell"

cat << EOF > /etc/nginx/sites-enabled/default
server {
    server_name test-link.tk;
        location / {
                proxy_pass http://localhost:8080;
        }
}
EOF

certbot --nginx --non-interactive --agree-tos -m $EMAIL -d test-link.tk
rm -rf /var/lib/tomcat9/webapps/ROOT/*
chown gitlab-runner:gitlab-runner /var/lib/tomcat9/webapps/ROOT
